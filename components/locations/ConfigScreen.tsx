import React, {useCallback, useState, useEffect, ChangeEvent} from 'react';
import {ConfigAppSDK} from '@contentful/app-sdk';
import {Heading, Form, Box , Flex, FormControl, TextInput, Radio,Text} from '@contentful/f36-components';
import {css} from 'emotion';
import aioaicontypes1 from "../../assets/images/aioa-icon-type-1.svg";
import aioaicontype2 from "../../assets/images/aioa-icon-type-2.svg";
import aioaicontype3 from "../../assets/images/aioa-icon-type-3.svg";
import { /* useCMA, */ useSDK} from '@contentful/react-apps-toolkit';
import Image from "next/image";

export interface AppInstallationParameters {
    apiKey: string,
    color: string,
    position: string,
    icontype: string,
    iconsize: string
}

const ConfigScreen = () => {
    const [isValid, setIsValid] = useState(false);
    const [message, setMessage] = useState('');
    const [parameters, setParameters] = useState<AppInstallationParameters>({
        apiKey: "",
        color: "0033a3",
        position: "bottom_right",
        icontype: 'aioa-icon-type-1',
        iconsize: 'aioa-big-icon'
    });
    const sdk = useSDK<ConfigAppSDK>();
    const [iconsize, setIconSize] = useState(aioaicontypes1);
    /*
       To use the cma, inject it as follows.
       If it is not needed, you can remove the next line.
    */
    // const cma = useCMA();

    const onConfigure = useCallback(async () => {
        // This method will be called when a user clicks on "Install"
        // or "Save" in the configuration screen.
        // for more details see https://www.contentful.com/developers/docs/extensibility/ui-extensions/sdk-reference/#register-an-app-configuration-hook

        // Get current the state of EditorInterface and other entities
        // related to this app installation
        const currentState = await sdk.app.getCurrentState();


        return {
            // Parameters to be persisted as the app configuration.
            parameters,
            // In case you don't want to submit any update to app
            // locations, you can just pass the currentState as is
            targetState: currentState,
        };
    }, [parameters, sdk]);

    useEffect(() => {
        sdk.app.onConfigure(() => onConfigure());
    }, [sdk, onConfigure]);

    useEffect(() => {
        (async () => {
            // Get current parameters of the app.
            // If the app is not installed yet, `parameters` will be `null`.
            const currentParameters: AppInstallationParameters | null = await sdk.app.getParameters();
            if (currentParameters) {
                setParameters(currentParameters);
                if(currentParameters.apiKey!=''){
                    validateAPIKEY(currentParameters.apiKey);
                    if (currentParameters.icontype == "aioa-icon-type-2") {
                        setIconSize(aioaicontype2);
                    } else if (currentParameters.icontype == "aioa-icon-type-3") {
                        setIconSize(aioaicontype3);
                    } else if(currentParameters.icontype == "aioa-icon-type-1") {
                        setIconSize(aioaicontypes1);
                    }
                }
            }
            sdk.app.setReady();
        })();
    }, [sdk]);
    const onIconChange = (e:any)=> {
        setParameters({...parameters, icontype: e.target.value})
        if (e.target.value == "aioa-icon-type-2") {
            setIconSize(aioaicontype2);
        } else if (e.target.value == "aioa-icon-type-3") {
            setIconSize(aioaicontype3);
        } else {
            setIconSize(aioaicontypes1);
        }

    }
    const checkapiKey = (e:ChangeEvent<HTMLInputElement>) => {
        setParameters({...parameters, apiKey: e.target.value});
        setTimeout(function(){
            validateAPIKEY(e.target.value);
        },500);

    }
    const validateAPIKEY=(key:string)=>{
        var formdata = new FormData();
        formdata.append("token", key);
        formdata.append("SERVER_NAME", '');
        const fetchOptions = {
            method: "post",
            body: formdata
        };

        fetch("https://www.skynettechnologies.com/add-ons/license-api.php", fetchOptions).then(result => result.json()).then(res => {
            if (res.valid == true) {
                setIsValid(true);
                setMessage('');


            } else {
                setIsValid(false);
                if(key!="")
                setMessage('Invalid API key');
                //setParameters({...parameters, position: ""});
            }
        });
    }
    return (
        <Flex flexDirection="column" className={css({margin: '80px', maxWidth: '800px'})}>

            <Form>
                <Box className="all-in-one-accessibility-wrap">
                    <Box className="accessibility-settings">
                        <Heading>All in One Accessibility Settings:</Heading>
                        <Box className="all-one-accessibility-form">
                            <FormControl className="mb-30 row">
                                <FormControl.Label className="col-sm-3 col-form-label">License key required for full version:</FormControl.Label>
                                <Box className="col-sm-9">
                                    <TextInput
                                        value={parameters.apiKey}
                                        type='text'
                                        onChange={checkapiKey}
                                    />
                                    <Box className={`form-text ${isValid ? "d-none" : null}`}>  Please <a href="https://www.skynettechnologies.com/add-ons/cart/?add-to-cart=116&variation_id=117&quantity=1&utm_medium=contentful-module&utm_campaign=purchase-plan" target="_blank">Upgrade</a> to full version of All in One Accessibility Pro
                                    </Box>
                                    <Box className='form-text text-danger'>{message}</Box>
                                </Box>
                            </FormControl>

                            <FormControl className={`mb-30 row`}>
                                <FormControl.Label htmlFor="inputPassword" className="col-sm-3 col-form-label">Hex color code:</FormControl.Label>
                                <Flex className="col-sm-9">
                                    <TextInput
                                        value={parameters.color}
                                        className="form-control" id="colorcode" name="colorcode"
                                        onChange={(e) => setParameters({...parameters, color: e.target.value})}
                                    />
                                </Flex>
                                <Box  className="col-sm-3"></Box>
                                <Box className="form-text col-sm-9">You can customize the ADA Widget color. For example: FF5733</Box>
                            </FormControl>

                            <FormControl className={`mb-30 row`}>

                                <FormControl.Label className="fcol-sm-12 col-form-label">Where would you like to place the accessibility icon on your site?:
                                </FormControl.Label>
                                <Box className="col-sm-12 three-col">

                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-top-left" name="position"
                                               value="top_left" onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               isChecked={parameters.position == 'top_left' ? true : false}
                                               className="form-radio">Top left</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-top-center" name="position" onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               value="top_center" isChecked={parameters.position == 'top_center' ? true : false}
                                               className="form-radio">Top Center</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-top-right" name="position" onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               value="top_right" isChecked={parameters.position == 'top_right' ? true : false}
                                               className="form-radio">Top Right</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-middel-left" name="position" onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               value="middle_left" isChecked={parameters.position == 'middle_left' ? true : false}
                                               className="form-radio">Middle left</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-middle-right"  name="position"
                                               value="middle_right" onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               isChecked={parameters.position == 'middle_right' ? true : false}
                                               className="form-radio">Middle Right</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-bottom-left" name="position" onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               value="bottom_left" isChecked={parameters.position == 'bottom_left' ? true : false}
                                               className="form-radio">
                                            Bottom left</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio  id="edit-position-bottom-center"
                                               onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               name="position" value="bottom_center" isChecked={parameters.position == 'bottom_center' ? true : false}
                                               className="form-radio">Bottom Center</Radio>
                                    </Box>
                                    <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                        <Radio id="edit-position-bottom-right" name="position"
                                               onChange={(e) => setParameters({...parameters, position: e.target.value})}
                                               value="bottom_right" isChecked={parameters.position == 'bottom_right' ? true : false}
                                               className="form-radio">Bottom Right</Radio>
                                    </Box>
                                </Box>
                            </FormControl>
                            <FormControl className={`icon-type-wrapper row ${isValid ? null : "d-none"}`}>
                                <FormControl.Label className="fcol-sm-12 col-form-label">Select icon type:</FormControl.Label>
                                <Box className="col-sm-12">
                                    <Box className="row">
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-type-1" name="aioa_icon_type" value="aioa-icon-type-1" className="form-radio"
                                                       isChecked={parameters.icontype == 'aioa-icon-type-1' ? true : false}
                                                       onChange={onIconChange}>
                                                    <Image src={aioaicontypes1.src} alt={parameters.icontype} width="65" height="65"/>
                                                    <Text className="visually-hidden">Type 1</Text>
                                                </Radio>

                                            </Box>
                                        </Box>
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-type-2" name="aioa_icon_type" value="aioa-icon-type-2"
                                                       isChecked={parameters.icontype == 'aioa-icon-type-2' ? true : false}
                                                       className="form-radio" onChange={onIconChange}>
                                                    <Image src={aioaicontype2.src} alt={parameters.icontype} width="65" height="65"/>
                                                    <Text className="visually-hidden">Type 2</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-type-3" name="aioa_icon_type"
                                                       value="aioa-icon-type-3"
                                                       isChecked={parameters.icontype == 'aioa-icon-type-3' ? true : false}
                                                       className="form-radio" onChange={onIconChange}>
                                                    <Image src={aioaicontype3.src} alt={parameters.icontype} width="65" height="65"/>
                                                    <Text className="visually-hidden">Type 3</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                            </FormControl>
                            <FormControl className={`icon-size-wrapper row ${isValid ? null : "d-none"}`}>
                                <FormControl.Label className="fcol-sm-12 col-form-label">Select icon size:</FormControl.Label>
                                <Box className="col-sm-12">
                                    <Box className="row">
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-size-big" name="aioa_icon_size" value="aioa-big-icon"
                                                       isChecked={parameters.iconsize == 'aioa-big-icon' ? true : false}
                                                       onChange={(e) => setParameters({...parameters, iconsize: e.target.value})}
                                                       className="form-radio">
                                                    <Image src={iconsize.src} alt={parameters.iconsize} width="75" height="75"/>
                                                    <Text className="visually-hidden">Big</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-size-medium" name="aioa_icon_size" value="aioa-medium-icon"
                                                       isChecked={parameters.iconsize == 'aioa-medium-icon' ? true : false}
                                                       onChange={(e) => setParameters({...parameters, iconsize: e.target.value})}
                                                       className="form-radio">
                                                    <Image src={iconsize.src} alt={parameters.iconsize}  width="65" height="65"/>
                                                    <Text className="visually-hidden">Medium</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-size-default" name="aioa_icon_size"
                                                       value="aioa-default-icon"
                                                       isChecked={parameters.iconsize == 'aioa-default-icon' ? true : false}
                                                       onChange={(e) => setParameters({...parameters, iconsize: e.target.value})}
                                                       className="form-radio">

                                                    <Image src={iconsize.src} alt={parameters.iconsize}  width="55" height="55"/>
                                                    <Text className="visually-hidden">Default</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-size-small" name="aioa_icon_size" value="aioa-small-icon"
                                                       isChecked={parameters.iconsize == 'aioa-small-icon' ? true : false}
                                                       onChange={(e) => setParameters({...parameters, iconsize: e.target.value})}
                                                       className="form-radio">

                                                    <Image src={iconsize.src} alt={parameters.iconsize}  width="45" height="45"/>
                                                    <Text className="visually-hidden">Small</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                        <Box className="col-auto mb-30">
                                            <Box className="js-form-item form-item js-form-type-radio form-type-radio js-form-item-position form-item-position">
                                                <Radio id="edit-size-extra-small" name="aioa_icon_size" value="aioa-extra-small-icon"
                                                       isChecked={parameters.iconsize == 'aioa-extra-small-icon' ? true : false}
                                                       onChange={(e) => setParameters({...parameters, iconsize: e.target.value})}
                                                       className="form-radio">

                                                    <Image src={`${iconsize.src}`} alt={parameters.iconsize}  width="35" height="35"/>
                                                    <Text className="visually-hidden">Extra Small</Text>
                                                </Radio>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                            </FormControl>


                        </Box>
                    </Box>

                </Box>

            </Form>


        </Flex>
    );
};

export default ConfigScreen;
